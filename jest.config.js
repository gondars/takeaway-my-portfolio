//jest.config.js
module.exports = {
    'moduleFileExtensions': [
        'js',
        'vue',
        'ts'
    ],

    'modulePaths': [
        '<rootDir>/src',
        '<rootDir>/node_modules'
    ],

    'transform': {
        '^.+\\.(js|ts)$': '<rootDir>/node_modules/ts-jest',
        '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest'
    },

    'snapshotSerializers': [
        '<rootDir>/node_modules/jest-serializer-vue'
    ],

    'moduleNameMapper': {
        '^@/(.*)$': '<rootDir>/src/$1',
        '\\.(css|less)$': '<rootDir>/tests/unit/utils/styleMock.js'
    },

    'transformIgnorePatterns': [
    ],

    preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel'
};

console.log('jest config loaded');
