import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators';
import IProject from '@/interfaces/IProject';
import axios, {AxiosResponse} from "axios";
import store from "@/store";

export interface IProjectsState {
    projects: IProject[]
}

/**
 * Projects Store Module, used for managing projects' data
 */
@Module({ dynamic: true, store, name: 'projects'})
class Projects extends VuexModule implements IProjectsState {
    /**
     * Projects field
     * @type IProject[]
     */
    public projects: IProject[] = [];

    /**
     * Mutation for setting and replacing the projects
     * @param payload type IProject[]
     * @constructor
     * @returns void
     */
    @Mutation
    public SET_PROJECTS(payload: IProject[]): void {
        this.projects = payload;
    }

    /**
     * Mutation for adding a project into the projects array
     * @param payload type: Iproject
     * @constructor
     */
    @Mutation
    public ADD_PROJECT(payload: IProject): void {
        this.projects.push(payload);
    }

    /**
     * Mutation for deleting a project from the projects array by id
     * @param id
     * @type number
     * @constructor
     */
    @Mutation
    public DELETE_PROJECT(id: number): void {
        let projectsAfterDelete: IProject[] = [];
        this.projects.forEach((project) => {
            if (project.id !== id) {
                projectsAfterDelete.push(project);
            }
        });

        this.projects = projectsAfterDelete;

    }

    /**
     * Getter for projects
     */
    get getProjects(): IProject[] {
        return this.projects
    }

    /**
     * Action for adding projects, assigning them an id
     * and invoking mutation ADD_PROJECT
     * @param payload
     * @type IProject
     */
    @Action
    public addProject(payload: IProject): void {
        const project: IProject = {
            id: Math.floor(Math.random() * 10000000),
            name: payload.name,
            description: payload.description
        };
        this.ADD_PROJECT(project);
    }

    /**
     * Action for deleting a project by id
     * @param id
     * @type number
     */
    @Action
    public deleteProject(id: number): void {
        this.DELETE_PROJECT(id);
    }

    /**
     * Action for importing projects from an url,
     * invokes add project action on success
     * @param path
     * @type string
     */
    @Action
    async importProjectsFromPath(path: string): Promise<void> {
        await axios.get(path).then((response: AxiosResponse<any>) => {
            response.data.forEach((entry: any) => {
                this.addProject(entry);
            });
        }).catch((e) => {
            console.log(e);
        })
    }

}
export const ProjectsModule = getModule(Projects);
