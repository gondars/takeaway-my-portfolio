import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators';
import store from "@/store";
import axios, {AxiosResponse} from "axios";
import IUser from "@/interfaces/IUser";


export interface IUserState {
    user: IUser
}

/**
 * User Store Module
 */
@Module({ dynamic: true, store, name: 'user'})
class User extends VuexModule {
    /**
     * User field that stores the info
     * @type IUser
     */
    public user: IUser = { title: '', slogan: '', about: ''};

    /**
     * Mutation for setting user state
     * @param userInfo
     * @type IUser
     * @constructor
     */
    @Mutation
    private SET_USER_INFO(userInfo: IUser) {
        this.user = userInfo;
    }

    /**
     * Getter for user state
     * @returns IUser
     */
    get getUser(): IUser {
        return this.user;
    }

    /**
     * Action for importing users from an url,
     * invokes SET_USER_INFO mutation
     * @param path
     * @type string
     */
    @Action
    async importUserFromPath(path: string): Promise<void> {
        await axios.get(path).then((response: AxiosResponse<any>) => {
            const userInfo: IUser = {
                title: response.data.title,
                slogan: response.data.slogan,
                about: response.data.about,
            };

            this.SET_USER_INFO(userInfo)
        }).catch((e) => {
            console.log(e);
        })
    }

}
export const UserModule = getModule(User);
