import Vue from 'vue';
import Vuex from 'vuex';
import { IUserState } from '@/store/modules/user';
import { IProjectsState } from '@/store/modules/projects';

Vue.use(Vuex);

export interface IRootState {
    projects: IProjectsState,
    user: IUserState
}

export default new Vuex.Store<IRootState>({});
