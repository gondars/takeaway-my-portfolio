import Component from "vue-class-component";

Component.registerHooks([
    'beforeRouteEnter',
    'beforeRouteLeave',
    'beforeRouteUpdate' // for vue-router 2.2+
]);

import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store';
import { BootstrapVue, IconsPlugin, NavbarPlugin, FormInputPlugin, ButtonPlugin} from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;

Vue.use(BootstrapVue)
    .use(IconsPlugin)
    .use(NavbarPlugin)
    .use(FormInputPlugin)
    .use(ButtonPlugin);

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');
