import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import {ProjectsModule} from "@/store/modules/projects";
import {UserModule} from "@/store/modules/user";

Vue.use(VueRouter);


const routes: Array<RouteConfig> = [
    {
        path: '*',
        name: 'Home',
        component: () => import('@/views/Home.vue')
    }, {
        path: '/about',
        name: 'About',
        component: () => import('@/views/About.vue')
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(async (to, from, next) => {
    if (!from.name) {
        await ProjectsModule.importProjectsFromPath('/projects.json').then(async () => {
            await UserModule.importUserFromPath('/user.json').then(() => {
                next();
            });
        });
    } else {
        next();
    }
});
export default router;
