export default interface IUser {
    title: string,
    slogan: string,
    about: string
}
