My Portfolio
=== 

My Portfolio is an application for managing your projects and about info. You can add, delete and even import projects from anywhere! Application was made  using [Vue.js](https://vuejs.org/).

## Prerequisites
You will need [Node.js](https://nodejs.org) version 6.0 or greater installed on your system.

## Setup

Get the code by either cloning this repository using git

    > git clone https://gondars@bitbucket.org/gondars/takeaway-my-portfolio.git

... or downloading source code code as a zip archive.

Once downloaded, open the terminal in the project directory, and continue with:

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### JEST unit tests 
```
npm run test
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Configuring My Portfolio
If you'd like to edit the initial information about the user and the projects, you can edit them at `public\user.json` and `public\projects.json`. Please keep the required format for them:

User:
```
Object user: IUser {
    title: "page title (String),
    slogan: "page slogan (String),
    about: "about info (String)
}
```

Projects: 
```
Array projects: IProject[] [{
    name: "name of project (String)",
    description: "description of project (String)"
}]
```

For a more detailed explanation on how this example works, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
