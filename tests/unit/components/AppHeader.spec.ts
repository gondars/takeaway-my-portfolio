import {shallowMount, Wrapper} from '@vue/test-utils'
import AppHeader from '@/components/AppHeader.vue'

describe('AppHeader.vue', () => {
    it('renders title and slogan when passed', () => {
        const wrapper: Wrapper<AppHeader> = shallowMount(AppHeader, {
            propsData: {
                title: 'titleText',
                slogan: 'sloganText'
            }
        });
        let headerContainer = wrapper.find('.header-container');
        let title = headerContainer.find('.title');
        let slogan = headerContainer.find('.slogan');

        expect(title.text()).toEqual(wrapper.props().title);
        expect(slogan.text()).toEqual(wrapper.props().slogan);

    })
});
