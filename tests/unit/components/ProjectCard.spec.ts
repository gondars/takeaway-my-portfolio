import {shallowMount, Wrapper} from '@vue/test-utils';
import ProjectCard from '@/components/ProjectCard.vue';

describe('ProjectCard.vue', () => {
    const wrapper: Wrapper<ProjectCard> = shallowMount(ProjectCard, {
        propsData: {
            project: {
                id: 44,
                name: 'projectName',
                description: 'projectDescription'
            }
        }
    });

    it('renders name and description when passed', () => {
        const nameContent = wrapper.find('.header-content');
        const desciptionContent = wrapper.find('.description-content');

        expect(nameContent.text()).toEqual(wrapper.props().project.name);
        expect(desciptionContent.text()).toEqual(wrapper.props().project.description);
    });

    it('clicking on delete emits event with payload', () => {
        const deleteButton = wrapper.find('.delete-project');
        deleteButton.trigger('click');

        expect(wrapper.emitted()).toEqual({'delete':[[ wrapper.props().project]]});
    })
});
