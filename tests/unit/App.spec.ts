import {createLocalVue, shallowMount, Wrapper} from '@vue/test-utils'
import App from '@/App.vue'
import Vuex from "vuex";
import IUser from "@/interfaces/IUser";

const localVue = createLocalVue();
localVue.use(Vuex);

const user: IUser ={
    title: 'title',
    slogan: 'slogan',
    about: 'aboutText'
};

describe('App.vue', () => {
    it('checks if user is synced from store', () => {
        const store = new Vuex.Store({
            modules: {
                user: {
                    getters: {
                        getUser: () => user
                    }
                }
            }
        });

        const wrapper: Wrapper<App> = shallowMount(App, { store, localVue });
        // @ts-ignore
        expect(wrapper.vm.userInfo).toEqual(user);
    })
})
