import {createLocalVue, shallowMount, Wrapper} from '@vue/test-utils'
import Vuex from 'vuex';
import About from '@/views/About.vue'
import IUser from "@/interfaces/IUser";

const localVue = createLocalVue();
localVue.use(Vuex);

const user: IUser ={
    title: 'title',
    slogan: 'slogan',
    about: 'aboutText'
};

describe('About.vue', () => {
    it('renders about when passed', () => {
        const wrapper: Wrapper<About> = shallowMount(About, {
            computed: {
                userInfo: jest.fn(() => { return user;})
            }
        });
        let aboutContainer = wrapper.find('.about');

        expect(aboutContainer.text()).toEqual(user.about);
    });
    it('mock store and get userInfo', () => {
        const store = new Vuex.Store({
            modules: {
                user: {
                    getters: {
                        getUser: () => user
                    }
                }
            }

        });

        const wrapper: Wrapper<About> = shallowMount(About, { store, localVue });
            // @ts-ignore
        expect(wrapper.vm.userInfo).toEqual(user);
    })
});
