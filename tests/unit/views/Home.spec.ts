import {createLocalVue, shallowMount, Wrapper} from '@vue/test-utils'
import Vuex from 'vuex';
import Home from '@/views/Home.vue'
import IProject from "@/interfaces/IProject";

const localVue = createLocalVue();
localVue.use(Vuex);

const projects: IProject[] = [
    {
        id: 0,
        name: 'project1',
        description: 'project1'
    },
    {
        id: 1,
        name: 'project2',
        description: 'project2'
    },
    {
        id: 2,
        name: 'project3',
        description: 'project3'
    }
];

describe('Home.vue', () => {
    const store = new Vuex.Store({
        modules: {
            projects: {
                getters: {
                    getProjects: () => projects
                },
                actions: {
                    addProject: jest.fn(),
                    deleteProject: jest.fn()
                }
            }
        },
    });

    const dispatchSpy = jest.spyOn(store,'dispatch');
    const wrapper: Wrapper<Home> = shallowMount(Home, {
        store,
        localVue
    });

    it('mock store projects sync', () => {
        // @ts-ignore
        expect(wrapper.vm.projects).toEqual(projects);
    });

    it('addProject called when button is clicked', () => {
        // @ts-ignore
        wrapper.vm.addProject = jest.fn();
        wrapper.vm.$nextTick(() => {
            const addButton = wrapper.find('.add-project-button');
            addButton.trigger('click');

            // @ts-ignore
            expect(wrapper.vm.addProject).toHaveBeenCalled();
        })
    });

    it('mock add project click and dispatch', () => {
        const addButton = wrapper.find('.add-project-button');
        addButton.trigger('click');

        wrapper.vm.$nextTick(() => {
            expect(dispatchSpy).toHaveBeenCalledWith("addProject", {"description": "", "id": 0, "name": ""});

            // @ts-ignore
            expect(wrapper.vm.project).toEqual({id: 0, name: '', description: ''})
        })

    });

    it('mock delete project called when delete is called on projectcard', () => {
        // @ts-ignore
        wrapper.vm.deleteProject = jest.fn();
        wrapper.vm.$nextTick().then(() => {
            const projectCard = wrapper.find('.project-card');
            expect(projectCard.props().project).toEqual(projects[0]);
            projectCard.vm.$emit('delete', projects[0]);

            // @ts-ignore
            expect(wrapper.vm.deleteProject).toHaveBeenCalled();
        });
    });

    it('mock dispatch on deleteproject', () => {
        // @ts-ignore
        wrapper.vm.deleteProject(projects[0]);
        wrapper.vm.$nextTick(() => {
            expect(dispatchSpy).toHaveBeenCalledWith("deleteProject", projects[0].id);
        });
    });

});
